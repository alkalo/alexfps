﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gundamage : MonoBehaviour {

	public float damage = 10f;
	public float range = 100f;

	public float fireRate = 15f;
	public float impactforce = 30f;

	public Camera fpscam;

	public ParticleSystem flash;

	public GameObject impacteffect;

	private float nextTimetofire = 0f;
	private SoundPlayer sound;

	void Start(){
		sound = GetComponentInChildren<SoundPlayer>();
	}
	void Update () {
		if (Input.GetButtonDown("Fire1") && Time.time >= nextTimetofire)
		{
			nextTimetofire = Time.time + 5f/fireRate;
			Shoot();
		}
	}

	void Shoot () 
	{

		flash.Play();
		sound.Play(0, 1);

		RaycastHit hit;
		if (Physics.Raycast(fpscam.transform.position, fpscam.transform.forward, out hit, range))
		{
			Debug.Log(hit.transform.name);
			
			hit.transform.GetComponent<EnemyBehaviour>();
			hit.transform.GetComponent<EnemyBehaviourtwo>();
			
			EnemyBehaviour target = hit.transform.GetComponent<EnemyBehaviour>();
			EnemyBehaviourtwo target2 = hit.transform.GetComponent<EnemyBehaviourtwo>();
			
			if (target != null)
			{
				target.TakeDamage(damage);
			}
			if (target2 != null)
			{
				target2.TakeDamage(damage);
			} 

			if (hit.rigidbody != null)
			{
				hit.rigidbody.AddForce(-hit.normal * impactforce);
			}

			GameObject impactGO = Instantiate(impacteffect, hit.point, Quaternion.LookRotation(hit.normal));
			Destroy (impactGO, 2f);
		}
	}
}
