﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapController : MonoBehaviour {
	public Transform playerTransform;
	public float offset_y = 1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = playerTransform.position + Vector3.up * 5f * offset_y ;
	}
}
