﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MusicController : MonoBehaviour {
	
	public Animator musicWindow;

	public Dropdown myDropdown;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void CloseMusicWindow () {
		musicWindow.SetBool("Open",false);
	}

	public void OpenMusicWindow () {
		musicWindow.SetBool("Open",true);
	}

}
